import React, { useContext } from "react";
import { Route, Redirect } from "react-router-dom";

import AuthContext from "../contexts/auth";

const PublicRoute = ({ component: Component, restricted, ...rest }) => {
	const { auth } = useContext(AuthContext);

	return (
		<Route
			{...rest}
			render={(props) =>
				auth && restricted ? <Redirect to="/" /> : <Component {...props} />
			}
		/>
	);
};

export default PublicRoute;
