import React from "react";
import { BrowserRouter, Switch } from "react-router-dom";

import PrivateRoute from "./PrivateRoute";
import PublicRoute from "./PublicRoutes";

import Login from "../pages/Login";
import Home from "../pages/Home";
import Details from "../pages/Details";
import ScrollToTop from "../utils/scrollToTop";

const Routes = () => {
	return (
		<BrowserRouter>
			<>
				<ScrollToTop />
				<Switch>
					<PrivateRoute component={Home} path="/" exact />
					<PrivateRoute component={Details} path="/details" />
					<PublicRoute
						restricted={true}
						component={Login}
						path="/login"
						exact
					/>
				</Switch>
			</>
		</BrowserRouter>
	);
};

export default Routes;
