import React, { createContext, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import api from "../services/api";

const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
	const [user, setUser] = useState(null);
	const [wrongCredentials, setWrongCredentials] = useState(false);
	const [loading, setLoading] = useState(false);
	const [headers, setHeaders] = useState({});

	const history = useHistory();

	async function handleLogin(data) {
		setLoading(true);
		try {
			const { data: res, headers } = await api.post(
				"/users/auth/sign_in",
				data
			);
			if (res.success) {
				setUser(res.investor);
				setHeaders(headers);
				sessionStorage.setItem("user", JSON.stringify(res.investor));
				sessionStorage.setItem("headers", JSON.stringify(headers));
			}
		} catch {
			setWrongCredentials(true);
		}
		setLoading(false);
	}

	useEffect(() => {
		let user = sessionStorage.getItem("user");
		user && setUser(JSON.parse(user));

		let headers = sessionStorage.getItem("headers");
		headers && setUser(JSON.parse(headers));

		if (headers) {
			if (Date.now() <= headers.expiry * 1000) {
				sessionStorage.removeItem("user");
				sessionStorage.removeItem("headers");
				history.push("/login");
			}
		}

		return () => {
			setUser(null);
			setHeaders({});
		};
	}, [history]);

	useEffect(() => {
		api.defaults.headers = {
			"access-token": headers["access-token"],
			client: headers["client"],
			uid: headers["uid"],
		};
		return () => {
			delete api.defaults.headers;
		};
	}, [headers]);

	return (
		<AuthContext.Provider
			value={{ auth: !!user, user, wrongCredentials, handleLogin, loading }}
		>
			{children}
		</AuthContext.Provider>
	);
};

export default AuthContext;
