import React, { useEffect, useState } from "react";

import { useLocation, Link } from "react-router-dom";

import arrowLeft from "../../assets/icons/arrow-left.png";

import "./styles.css";

const Details = () => {
	const location = useLocation();

	const [enterprise, setEnterprise] = useState({});

	useEffect(() => {
		setEnterprise(location.enterprise);
		return () => {
			setEnterprise({});
		};
	}, [location]);

	return (
		<div className="container" id="details">
			<header className="header">
				<Link className="back" to="/">
					<img src={arrowLeft} alt="voltar" />
				</Link>
				{
					<div>
						<h1 className="text-5"> {enterprise.enterprise_name} </h1>
					</div>
				}
			</header>
			<main>
				<div className="enterprise-container">
					<div className="logo">E1</div>
					<p className="description text-9">{enterprise.description}</p>
				</div>
			</main>
		</div>
	);
};

export default Details;
