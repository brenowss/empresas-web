import React, { useContext, useState } from "react";

import AuthContext from "../../contexts/auth";

import "./styles.css";

import logo from "../../assets/images/logo-home.png";
import lockIcon from "../../assets/icons/ic-cadeado.png";
import mailIcon from "../../assets/icons/ic-email.png";
import eyeOpen from "../../assets/icons/eye-open.png";
import eyeClosed from "../../assets/icons/eye-closed.png";
import loadingGif from "../../assets/images/loading.gif";

const Login = () => {
	const { handleLogin, wrongCredentials, loading } = useContext(AuthContext);

	const [passwordVisible, setPasswordVisible] = useState(false);
	const [email, setEmail] = useState(null);
	const [password, setPassword] = useState(null);

	function handlePasswordVisibility(e) {
		e.preventDefault();
		setPasswordVisible(!passwordVisible);
	}

	async function signIn(e) {
		e.preventDefault();
		const data = {
			email,
			password,
		};

		handleLogin(data);
	}

	return (
		<div className="container" id="login">
			<div className="form-wrapper">
				<img src={logo} alt="Logo ioasys" className="logo-image" />
				<h3 className="text-10">
					BEM-VINDO AO <br /> EMPRESAS
				</h3>
				<p className="text">
					Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
				</p>
				<form action="" id="login-form">
					<div
						role="group"
						className={wrongCredentials ? "input-group error" : "input-group"}
					>
						<img src={lockIcon} alt="" className="input-icon" />
						<input
							required
							type="text"
							name="email"
							id="email"
							data-testid="email-input"
							placeholder="E-mail"
							onChange={(e) => setEmail(e.target.value)}
						/>
					</div>
					<div
						role="group"
						className={wrongCredentials ? "input-group error" : "input-group"}
					>
						<img src={mailIcon} alt="" className="input-icon" />
						<input
							required
							type={passwordVisible ? "text" : "password"}
							name="password"
							data-testid="password-input"
							id="password"
							placeholder="Senha"
							onChange={(e) => setPassword(e.target.value)}
						/>
						{passwordVisible ? (
							<img
								src={eyeClosed}
								alt="hidden password"
								className="input-icon"
								onClick={handlePasswordVisibility}
							/>
						) : (
							<img
								src={eyeOpen}
								alt="shown password"
								className="input-icon"
								onClick={handlePasswordVisibility}
							/>
						)}
					</div>

					{wrongCredentials && (
						<span className="error-message">
							Credenciais informadas são inválidas, tente novamente.
						</span>
					)}

					<button type="submit" className="btn" onClick={signIn} data-testid="submit-btn">
						ENTRAR
					</button>
				</form>
			</div>
			{loading && (
				<div className="loading-wrapper">
					<img src={loadingGif} alt="loading" />
				</div>
			)}
		</div>
	);
};

export default Login;
