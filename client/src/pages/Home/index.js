import React, { useState, useRef, useEffect } from "react";
import { Link } from "react-router-dom";

import api from "../../services/api";

import "./styles.css";

import logo from "../../assets/images/logo-nav.png";
import searchIcon from "../../assets/icons/ic-search.png";
import closeIcon from "../../assets/icons/ic-close.png";
import filterIcon from "../../assets/icons/filter.png";
import EnterpriseCard from "../../components/EnterpriseCard";

const Home = () => {
	const [searchFocus, setSearchFocus] = useState(false);
	const [headers, setHeaders] = useState({});
	const [query, setQuery] = useState(null);
	const [enterprises, setEnterprises] = useState([]);
	const [type, setType] = useState(null);
	const [filterBox, setFilterBox] = useState(false);

	const inputRef = useRef(0);
	const typesRef = useRef(0);
	const storedHeaders = sessionStorage.getItem("headers");

	const enterpriseTypes = [
		{
			title: "Fintech",
			value: "2",
		},
		{
			title: "Health",
			value: "3",
		},
		{
			title: "HR Tech",
			value: "6",
		},
		{
			title: "IT",
			value: "7",
		},
		{
			title: "Service",
			value: "8",
		},
		{
			title: "Software",
			value: "11",
		},
		{
			title: "Social",
			value: "13",
		},
		{
			title: "Food",
			value: "16",
		},
		{
			title: "Marketplace",
			value: "21",
		},
		{
			title: "Industry",
			value: "23",
		},
		{
			title: "Transport",
			value: "24",
		},
		{
			title: "IoT",
			value: "26",
		},
		{
			title: "Music",
			value: "27",
		},
		{
			title: "Green",
			value: "28",
		},
	];

	useEffect(() => {
		async function getHeaders() {
			let headers = await JSON.parse(storedHeaders);
			setHeaders(headers);
		}
		getHeaders();

		return () => {
			setHeaders({});
		};
	}, [storedHeaders]);

	useEffect(() => {
		async function handleSearch() {
			const {
				data: { enterprises },
			} = await api.get("/enterprises", {
				params: {
					name: query,
				},
				headers: {
					"access-token": headers["access-token"],
					uid: headers["uid"],
					client: headers["client"],
				},
			});
			setEnterprises(enterprises);
		}

		query && handleSearch();
	}, [query, headers]);

	useEffect(() => {
		async function handleSearchWithFilter() {
			try {
				const {
					data: { enterprises },
				} = await api.get("/enterprises", {
					params: {
						name: query,
						enterprise_types: type,
					},
					headers: {
						"access-token": headers["access-token"],
						uid: headers["uid"],
						client: headers["client"],
					},
				});
				setEnterprises(enterprises);
			} catch {
				alert("Ocorreu um erro com a sua requisição, desculpa :(. Tente reiniciar o seu servidor!")
			}
		}

		query && handleSearchWithFilter();
	}, [query, type, headers]);

	useEffect(() => {
		searchFocus && inputRef.current.focus();
	}, [searchFocus]);

	return (
		<div className="container" id="home">
			<header className="header">
				{!searchFocus && <img src={logo} alt="ioasys" />}
				<form action="" id="search-form">
					<div
						className={searchFocus ? "search-group focused" : "search-group"}
						role="group"
					>
						<button
							className="search btn"
							data-testid="search-btn"
							onClick={(e) => {
								e.preventDefault();
								setSearchFocus(!searchFocus);
							}}
						>
							<img src={searchIcon} alt="search" />
						</button>
						<input
							ref={inputRef}
							placeholder="Pesquisar"
							type="text"
							name="q"
							id="search"
							data-testid="search-field"
							onChange={(e) => setQuery(e.target.value)}
						/>
						<button
							className="close btn"
							onClick={(e) => {
								e.preventDefault();
								inputRef.current.value = "";
								inputRef.current.focus();
								setQuery(null);
								setEnterprises([]);
							}}
						>
							<img src={closeIcon} alt="close" />
						</button>
					</div>
				</form>
			</header>
			<main>
				{query && (
					<div className="filter">
						<button
							className="btn"
							onClick={(e) => {
								e.preventDefault();
								setFilterBox(!filterBox);
							}}
						>
							<div>
								<img src={filterIcon} alt="filter" />
								<span className="text-2">Filtrar empresas</span>
							</div>
						</button>
						<div
							className={filterBox ? "filter-options" : "filter-options hidden"}
						>
							<span>Tipo:</span>
							<select
								ref={typesRef}
								name="type"
								id="type"
								onChange={(e) => setType(e.target.value)}
								defaultValue=""
							>
								<option value="" disabled>
									Selecione
								</option>
								{enterpriseTypes.map((type) => (
									<option key={type.value} value={type.value}>
										{type.title}
									</option>
								))}
							</select>
							{type && (
								<button
									className="clear-filter"
									onClick={(e) => {
										e.preventDefault();
										setType(null);
										typesRef.current.value = "";
									}}
								>
									Remover
								</button>
							)}
						</div>
					</div>
				)}
				{enterprises.length !== 0 &&
					enterprises.map((enterprise) => (
						<Link
							key={enterprise.id}
							to={{
								pathname: "/details",
								enterprise: enterprise,
							}}
						>
							<EnterpriseCard enterprise={enterprise} />
						</Link>
					))}
				{enterprises.length === 0 && !query && !searchFocus && (
					<span className="text-3">Clique na busca para iniciar.</span>
				)}
				{enterprises.length === 0 && query && (
					<span className="text-3">
						Nenhuma empresa foi encontrada para a busca realizada.
					</span>
				)}
			</main>
		</div>
	);
};

export default Home;
