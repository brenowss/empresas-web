import React from "react";

import "./styles.css";

const EnterpriseCard = ({ enterprise }) => {
	return (
		<div className="card-container">
			<div className="enterprise-logo">E1</div>
			<div className="enterprise-info">
				<h1 className="text-6">{enterprise.enterprise_name}</h1>
				<h1 className="text-7">
					{enterprise.enterprise_type.enterprise_type_name}
				</h1>
				<h1 className="text-8">
					{enterprise.city} - {enterprise.country}
				</h1>
			</div>
		</div>
	);
};

export default EnterpriseCard;
